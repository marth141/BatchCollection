@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get all CAD design documents organized into Gemini folders for use with Gemini File Sync.

set	mypath=%cd%
set	mypath2=%mypath:~-7%
mkdir	.\"%mypath2% PV 1"\
copy	"%cd%\%mypath2% CAD ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% CP ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% DATA ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% ECOF ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% ENG ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% INS ?.pdf"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% PLACARD ?.pdf"					"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% REPORT ?.pdf"					"%cd%\%mypath2% PV 1\"	
REM		"%cd%\cad1.pdf"									"%cd%\%mypath2% PV 1\"	
REM		"%cd%\cad2.pdf"									"%cd%\%mypath2% PV 1\"	
REM		"%cd%\ecof1.pdf"								"%cd%\%mypath2% PV 1\"	
REM		"%cd%\ecof2.pdf"								"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (1).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (2).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (3).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (4).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (5).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (6).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (7).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (8).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\SSAA ? (9).pdf"							"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% CAD ?.dwg"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% CAD ?.bak"						"%cd%\%mypath2% PV 1\"	
copy	"%cd%\%mypath2% Design Tool ?.xlsx"				"%cd%\%mypath2% PV 1\"	
REM		"%cd%\%mypath2% EcoFasten Calculator ?.xlsx"	"%cd%\%mypath2% PV 1\"	
copy	"%cd%\*.png"									"%cd%\%mypath2% PV 1\"	
copy	"%cd%\*.bmp"									"%cd%\%mypath2% PV 1\"	

move	"%cd%\1line1.pdf"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\1line2.pdf"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\%mypath2% CAD ?.pdf"						"%cd%\Engineering-Permitting\CAD (All CAD Drawings)\"		
move	"%cd%\%mypath2% CP ?.pdf"						"%cd%\Engineering-Permitting\Customer welcome packet\"		
move	"%cd%\%mypath2% DATA ?.pdf"						"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\%mypath2% ECOF ?.pdf"						"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\%mypath2% ENG ?.pdf"						"%cd%\Engineering-Permitting\ENG (Engineering Pages)\"		
move	"%cd%\%mypath2% INS ?.pdf"						"%cd%\Engineering-Permitting\INS (Installers packet)\"		
move	"%cd%\%mypath2% PLACARD ?.pdf"					"%cd%\Engineering-Permitting\CAD (Placard)\"				
move	"%cd%\%mypath2% REPORT ?.pdf"					"%cd%\Engineering-Permitting\Report (Solmetric report)\"	
move	"%cd%\cad1.pdf"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\cad2.pdf"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\cad3.pdf"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\deslog.pdf"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\ecof1.pdf"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\ecof2.pdf"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\SSAA ? (1).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (2).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (3).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (4).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (5).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (6).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (7).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (8).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\SSAA ? (9).pdf"							"%cd%\Engineering-Permitting\SSAA\"							
move	"%cd%\%mypath2% CAD ?.dwg"						"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\%mypath2% CAD ?.bak"						"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\%mypath2% Design Tool ?.xlsx"				"%cd%\Engineering-Permitting\Pre-Design Templates\"			
move	"%cd%\%mypath2% EcoFasten Calculator ?.xlsx"	"%cd%\Engineering-Permitting\Pre-Design Templates\"			
move	"%cd%\Master\*"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\Master 1*\*"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\Master 2*\*"								"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\*.png"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\*.bmp"									"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\Electrical Installation Notes.pdf"		"%cd%\Engineering-Permitting\Misc\"							
move	"%cd%\Electrical Installation Notes.docx"		"%cd%\Engineering-Permitting\Misc\"							

pause

echo: Press any key to proceed with deletion.

rmdir	%cd%\Master
attrib +r "%cd%\%mypath2% SPA ?.pdf"
DEL		"%cd%\*.*"			
attrib -r "%cd%\%mypath2% SPA ?.pdf"

pause