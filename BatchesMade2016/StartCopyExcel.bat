@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is an alternative starter for AutoHotKey automation scripts. This script copies data between two Excel spreadsheets.

start "" "%USERPROFILE%\Desktop\CAD\1 Working\Design Docs\AutoHotkey3-22-16\AutoHotkey.exe" "%USERPROFILE%\Desktop\CAD\1 Working\Design Docs\AutoHotkey3-22-16\CopyExcel.ahk"