@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get all CAD design documents used to work on an account. Saves me a click or two.

set	mp=%USERPROFILE%\Desktop\CAD\1 Working\Release
set	ds=%USERPROFILE%\Desktop\CAD\1 Working
set	pre=%cd%\Engineering-Permitting\Pre-Design Templates

copy	"%mp%\UTHotKey.ini"							"%cd%\"
copy	"%mp%\UTHotKey.exe"							"%cd%\"
del	"%cd%\GetUTHotKeyINI.bat"

echo:
@echo Job complete
echo:
Pause