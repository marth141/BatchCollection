@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get all Solmetric Reports from the Solmetric Exported Reports folder. It renames them once it has them.

move /-y "%USERPROFILE%\Documents\Solmetric\SunEye\Exported Reports\*.*" "%cd%\"
ren  "??????? Report.pdf" "SSAA 1 (1)".pdf
ren  "??????? Report (2).pdf" "SSAA 1 (2)".pdf
ren  "??????? Report (3).pdf" "SSAA 1 (3)".pdf
ren  "??????? Report (4).pdf" "SSAA 1 (4)".pdf
ren  "??????? Report (5).pdf" "SSAA 1 (5)".pdf
ren  "??????? Report (6).pdf" "SSAA 1 (6)".pdf
ren  "??????? Report (7).pdf" "SSAA 1 (7)".pdf
ren  "??????? Report (8).pdf" "SSAA 1 (8)".pdf
ren  "??????? Report (9).pdf" "SSAA 1 (9)".pdf
ren  "??????? Report (10).pdf" "SSAA 1 (10)".pdf
ren  "??????? Report (11).pdf" "SSAA 1 (11)".pdf
@echo:
@echo Job Complete.
@echo:
pause