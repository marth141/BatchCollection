@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to rename all of my documents if I happened to name them as 1. This code is simply to rename documents to 2.

set	mypath=%cd%
set	mypath2=%mypath:~-7%
set pc= (55.9%)
set It=1
REN		"%mypath2% CAD ?.pdf"						"%mypath2% CAD %It%%pc%.pdf"						
REN		"%mypath2% CP ?.pdf"						"%mypath2% CP %It%%pc%.pdf"						
REN		"%mypath2% DATA ?.pdf"						"%mypath2% DATA %It%%pc%.pdf"						
REN		"%mypath2% ECOF ?.pdf"						"%mypath2% ECOF %It%%pc%.pdf"						
REN		"%mypath2% ENG ?.pdf"						"%mypath2% ENG %It%%pc%.pdf"						
REN		"%mypath2% INS ?.pdf"						"%mypath2% INS %It%%pc%.pdf"						
REN		"%mypath2% PLACARD ?.pdf"					"%mypath2% PLACARD %It%%pc%.pdf"					
REN		"%mypath2% REPORT ?.pdf"					"%mypath2% REPORT %It%%pc%.pdf"					
REN		"SSAA ? (1).pdf"							"SSAA %It%%pc% (1).pdf"							
REN		"SSAA ? (2).pdf"							"SSAA %It%%pc% (2).pdf"							
REN		"SSAA ? (3).pdf"							"SSAA %It%%pc% (3).pdf"							
REN		"SSAA ? (4).pdf"							"SSAA %It%%pc% (4).pdf"							
REN		"SSAA ? (5).pdf"							"SSAA %It%%pc% (5).pdf"							
REN		"SSAA ? (6).pdf"							"SSAA %It%%pc% (6).pdf"							
REN		"SSAA ? (7).pdf"							"SSAA %It%%pc% (7).pdf"							
REN		"SSAA ? (8).pdf"							"SSAA %It%%pc% (8).pdf"							
REN		"SSAA ? (9).pdf"							"SSAA %It%%pc% (9).pdf"							
REN		"%mypath2% CAD ?.dwg"						"%mypath2% CAD %It%%pc%.dwg"						
REN		"%mypath2% CAD ?.bak"						"%mypath2% CAD %It%%pc%.bak"						
REN		"%mypath2% Design Tool ?.xlsx"				"%mypath2% Design Tool %It%%pc%.xlsx"				
REN		"%mypath2% EcoFasten Calculator ?.xlsx"		"%mypath2% EcoFasten Calculator %It%%pc%.xlsx"		