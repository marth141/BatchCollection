@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to rename all of my documents if I happened to name them as 1. This code is simply to rename documents to 2.

set	mypath=%cd%
set	mypath2=%mypath:~-7%
REN		"%mypath2% CAD ?.pdf"						"%mypath2% CAD 2.pdf"						
REN		"%mypath2% CP ?.pdf"						"%mypath2% CP 2.pdf"						
REN		"%mypath2% DATA ?.pdf"						"%mypath2% DATA 2.pdf"						
REN		"%mypath2% ECOF ?.pdf"						"%mypath2% ECOF 2.pdf"						
REN		"%mypath2% ENG ?.pdf"						"%mypath2% ENG 2.pdf"						
REN		"%mypath2% INS ?.pdf"						"%mypath2% INS 2.pdf"						
REN		"%mypath2% PLACARD ?.pdf"					"%mypath2% PLACARD 2.pdf"					
REN		"%mypath2% REPORT ?.pdf"					"%mypath2% REPORT 2.pdf"					
REN		"SSAA ? (1).pdf"							"SSAA 2 (1).pdf"							
REN		"SSAA ? (2).pdf"							"SSAA 2 (2).pdf"							
REN		"SSAA ? (3).pdf"							"SSAA 2 (3).pdf"							
REN		"SSAA ? (4).pdf"							"SSAA 2 (4).pdf"							
REN		"SSAA ? (5).pdf"							"SSAA 2 (5).pdf"							
REN		"SSAA ? (6).pdf"							"SSAA 2 (6).pdf"							
REN		"SSAA ? (7).pdf"							"SSAA 2 (7).pdf"							
REN		"SSAA ? (8).pdf"							"SSAA 2 (8).pdf"							
REN		"SSAA ? (9).pdf"							"SSAA 2 (9).pdf"							
REN		"%mypath2% CAD ?.dwg"						"%mypath2% CAD 2.dwg"						
REN		"%mypath2% CAD ?.bak"						"%mypath2% CAD 2.bak"						
REN		"%mypath2% Design Tool ?.xlsx"				"%mypath2% Design Tool 2.xlsx"				
REN		"%mypath2% EcoFasten Calculator ?.xlsx"		"%mypath2% EcoFasten Calculator 2.xlsx"		