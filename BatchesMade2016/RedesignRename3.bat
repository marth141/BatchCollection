@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to rename all of my documents if I happened to name them as 1. This code is simply to rename documents to 2.

set	mypath=%cd%
set	mypath2=%mypath:~-7%
REN		"%mypath2% CAD ?.pdf"						"%mypath2% CAD 3.pdf"						
REN		"%mypath2% CP ?.pdf"						"%mypath2% CP 3.pdf"						
REN		"%mypath2% DATA ?.pdf"						"%mypath2% DATA 3.pdf"						
REN		"%mypath2% ECOF ?.pdf"						"%mypath2% ECOF 3.pdf"						
REN		"%mypath2% ENG ?.pdf"						"%mypath2% ENG 3.pdf"						
REN		"%mypath2% INS ?.pdf"						"%mypath2% INS 3.pdf"						
REN		"%mypath2% PLACARD ?.pdf"					"%mypath2% PLACARD 3.pdf"					
REN		"%mypath2% REPORT ?.pdf"					"%mypath2% REPORT 3.pdf"					
REN		"SSAA ? (1).pdf"							"SSAA 3 (1).pdf"							
REN		"SSAA ? (2).pdf"							"SSAA 3 (2).pdf"							
REN		"SSAA ? (3).pdf"							"SSAA 3 (3).pdf"							
REN		"SSAA ? (4).pdf"							"SSAA 3 (4).pdf"							
REN		"SSAA ? (5).pdf"							"SSAA 3 (5).pdf"							
REN		"SSAA ? (6).pdf"							"SSAA 3 (6).pdf"							
REN		"SSAA ? (7).pdf"							"SSAA 3 (7).pdf"							
REN		"SSAA ? (8).pdf"							"SSAA 3 (8).pdf"							
REN		"SSAA ? (9).pdf"							"SSAA 3 (9).pdf"							
REN		"%mypath2% CAD ?.dwg"						"%mypath2% CAD 3.dwg"						
REN		"%mypath2% CAD ?.bak"						"%mypath2% CAD 3.bak"						
REN		"%mypath2% Design Tool ?.xlsx"				"%mypath2% Design Tool 3.xlsx"				
REN		"%mypath2% EcoFasten Calculator ?.xlsx"		"%mypath2% EcoFasten Calculator 3.xlsx"		