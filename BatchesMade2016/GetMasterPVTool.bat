@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get the Master 1-Line template from the P:\ drive. This is because the 1-Line template is updated often and
REM	is crucial to completing accounts in the CAD department.


mkdir	"%USERPROFILE%\Desktop\CAD\1 Working\PVTool\"
copy	"P:\Departments\Solar\Electric Engineering\0 Master 1-line Template\Master PVDesignTool\*"		"%cd%\PVTool"