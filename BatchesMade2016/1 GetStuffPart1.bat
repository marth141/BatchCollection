@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get all CAD design documents used to work on an account. Saves me a click or two.

set	mp=%USERPROFILE%\Desktop\CAD\1 Working\Design Docs
set ds=%USERPROFILE%\Desktop\CAD\1 Working
set ep=%cd%\Engineering-Permitting\Pre-Design Templates

REM copy	"%mp%\UT*.dwg"						"%cd%\"
REM copy	"%mp%\Fronius*.xlsx"				"%cd%\"
copy	"%mp%\Design Tool*.xlsx"				"%cd%\"
copy	"%ep%\*IC*.xlsx"					"%cd%\"
copy	"%ep%"\*UPDATED*.xlsx"				"%cd%\"
REM copy	"%mp%\EcoFasten Calculator*.xlsx"	"%cd%\"
copy	"%mp%\RenameTool.bat"				"%cd%\"
copy	"%mp%\SSAA1Creator.bat"				"%cd%\"
copy	"%mp%\VivintReportGetter.lnk"		"%cd%\"
copy	"%mp%\GetUTHotKeyINI.bat"			"%cd%\"
attrib +r "%cd%\*IC*.xlsx"
attrib +r "%cd%\*UPDATED*.xlsx"
pause
call	RenameTool.bat
pause
del		"RenameTool.bat"
pause
del		"GetStuffPart1.bat"


echo:
@echo Job complete
echo:
Pause