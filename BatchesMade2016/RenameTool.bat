@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to rename the first documents used to create every other document in a CAD account.

set	mypath=%cd%
set	mypath2=%mypath:~-7%

ren	"UT*.dwg"						"%mypath2% CAD 1.dwg"
ren	"UT*.bak"						"%mypath2% CAD 1.bak"
ren	"*Design Tool*.xlsx"			"%mypath2% Design Tool 1.xlsx"
ren	"EcoFasten Calculator*.xlsx"	"%mypath2% EcoFasten Calculator 1.xlsx"

echo:
@echo Job complete
echo:
Pause