@echo off

REM	DECLARATION - This code is not malicious. It's simply to make my job at Vivint Solar easier. I wouldn't attack the hand that feeds me.
REM	This code was declared final as of 06/03/2016.
REM	Version: 1.0.0

REM	This code is to be able to get all CAD design documents organized into Gemini folders for use with Gemini File Sync.

set	mypath=%cd%
set	mypath2=%mypath:~-7%
move	"%cd%\%mypath2% PV ?.zip"		"%cd%\Engineering-Permitting\Misc\"							
DEL /F /Q		"%cd%\*.*"			